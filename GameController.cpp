#include "GameController.h"

// Управление
void GameController::run() {
	sf::Event event;
	bool pause{};
	m_render->clockRestart();

	while (m_render->getWindow().isOpen())
	{
		while (m_render->getWindow().pollEvent(event))
		{
			// События окна
			switch (event.type)
			{
			case sf::Event::LostFocus:
				pause = true;
				break;
			case sf::Event::GainedFocus:
				pause = false;
				break;
			case sf::Event::Closed:
				m_render->getWindow().close(); 
				break;
			case sf::Event::KeyPressed:
				// События нажатия клавиш
				switch (event.key.code)
				{
				case sf::Keyboard::Escape:
					m_render->getWindow().close(); 
					break;
				}
			}
		}

		// Окно в режиме ожидания
		if (pause)
		{
			m_render->clockRestart();
		}
		else
		{
			m_render->render();
		}
	}
}