#pragma once

#include <SFML/Graphics.hpp>
#include "GameRender.h"

class GameController
{
private:
	GameModel* m_model;
	GameRender* m_render;

public:
	GameController(GameModel* model, GameRender* render)
		: m_model(model), m_render(render)
	{
	}
	~GameController()
	{
	}

	void run();
};
// взаимодействие 