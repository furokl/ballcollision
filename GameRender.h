#pragma once

#include <SFML/Graphics.hpp>

#include "GameModel.h"

class GameRender final
{
private:
	GameModel* m_model;
	sf::RenderWindow m_window;

	sf::Clock clock;
	sf::Time last_time;
	sf::Time current_time;

public:
	GameRender(GameModel* model)
		: m_model(model)
	{
		init(m_model);
	}
	~GameRender()
	{
	}
	GameRender(const GameRender&) = delete;
	GameRender& operator=(const GameRender&) = delete;

	sf::RenderWindow& getWindow() { return m_window; }
	sf::CircleShape gball;

	bool init(GameModel* model);
	void render();
	void drawFps(sf::RenderWindow& window, float fps);
	void drawBall(sf::RenderWindow& window, GameModel::Ball& ball);
	void clockRestart();
};