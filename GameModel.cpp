#include "GameModel.h"

// Инициализация GameModel
bool GameModel::init()
{
	srand(time(NULL));

	for (size_t i{}; i < getRandomNumber(MIN_BALLS, MAX_BALLS); ++i)
	{
		Ball newBall;
		m_balls.push_back(newBall);
	}

	return true;
}

// Перемещение шара
void GameModel::moveBall(Ball& ball, const float& delta_time)
{
	float dx = ball.m_direction.x * ball.m_speed * delta_time;
	float dy = ball.m_direction.y * ball.m_speed * delta_time;
	ball.m_position.x += dx;
	ball.m_position.y += dy;
}

// Столкновение шара со стеной
void GameModel::wallsCollision(Ball& ball) {
	if (ball.m_position.x + ball.m_radius > WINDOW_X ||
		ball.m_position.x - ball.m_radius < 0.f)
	{
		ball.m_direction.x = -ball.m_direction.x;
		++ball.m_collision_count;

		// Смещение шара, находящегося в левой/правой стене
		(ball.m_position.x + ball.m_radius > WINDOW_X) ?
			ball.m_position.x = WINDOW_X - ball.m_radius :
			ball.m_position.x = ball.m_radius;
	}

	else if (ball.m_position.y + ball.m_radius > WINDOW_Y ||
		ball.m_position.y - ball.m_radius < 0.f)
	{
		ball.m_direction.y = -ball.m_direction.y;
		++ball.m_collision_count;

		// Смещение шара, находящегося в верхней/нижней стене
		(ball.m_position.y + ball.m_radius > WINDOW_Y) ?
			ball.m_position.y = WINDOW_Y - ball.m_radius :
			ball.m_position.y = ball.m_radius;
	}
}

// Столкновение шара с шаром
void GameModel::ballsCollision() {
	for (size_t this_{}; this_ < m_balls.size(); ++this_)
	{
		std::vector<size_t> collided_balls{};

		for (size_t other_{}; other_ < m_balls.size(); ++other_)
		{
			float d = getDinstance(m_balls[this_], m_balls[other_]);

			// Если шар уже задействован в коллизии - переходим к следующей итерации
			if (this_ == other_) continue;
			for (auto& collided : collided_balls)
				if (collided == other_ || collided == this_) continue;

			if (d < m_balls[this_].m_radius + m_balls[other_].m_radius)
			{
				const float&
					v1_x = m_balls[this_].m_direction.x,
					v1_y = m_balls[this_].m_direction.y,
					v2_x = m_balls[other_].m_direction.x,
					v2_y = m_balls[other_].m_direction.y,
					m1 = m_balls[this_].m_radius * m_balls[this_].m_radius * constants::pi,
					m2 = m_balls[other_].m_radius * m_balls[other_].m_radius * constants::pi;

				m_balls[this_].m_direction.x = ((v1_x * (m1 - m2) + 2.f * m2 * v2_x) / (m1 + m2));
				m_balls[this_].m_direction.y = ((v1_y * (m1 - m2) + 2.f * m2 * v2_y) / (m1 + m2));
				m_balls[other_].m_direction.x = ((v2_x * (m2 - m1) + 2.f * m1 * v1_x) / (m1 + m2));
				m_balls[other_].m_direction.y = ((v2_y * (m2 - m1) + 2.f * m1 * v1_y) / (m1 + m2));

				// TO DO
				// !!! Шары застревают, ускоряются, игнорируют друг друга; смещение не помогает(?)

				collided_balls.push_back(this_);
				collided_balls.push_back(other_);
			}
		}
		collided_balls.clear();
	}
}

// Запуск всех коллизий
void GameModel::checkCollision() {
	ballsCollision();
	for (size_t this_{}; this_ < m_balls.size(); ++this_)
	{
		wallsCollision(m_balls[this_]);
	}
}

// Расчет расстояния между точками
const float& GameModel::getDinstance(Ball& ball_a, Ball& ball_b) const {
	float dx = ball_a.m_position.x - ball_b.m_position.x;
	float dy = ball_a.m_position.y - ball_b.m_position.y;
	return sqrt(dx * dx + dy * dy);
}

const int& GameModel::getWindow_X() const { return WINDOW_X; }
const int& GameModel::getWindow_Y() const { return WINDOW_Y; }
std::vector<GameModel::Ball>& GameModel::getBalls() { return m_balls; }