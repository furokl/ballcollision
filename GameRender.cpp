#include "GameRender.h"
#include "MiddleAverageFilter.h"

// Объект - счетчик fps, среднее значение на 100 циклов render
Math::MiddleAverageFilter<float, 100> fpscounter;

// Инициализация GameRender
bool GameRender::init(GameModel *model) {

	m_window.create(
		sf::VideoMode(model->getWindow_X(), model->getWindow_Y()),
		"Collision Balls",
		sf::Style::Titlebar | sf::Style::Close);
	//m_window.setFramerateLimit(60);
	//m_window.setVerticalSyncEnabled(true);

	clock = sf::Clock::Clock();
	last_time = clock.getElapsedTime();

	return true;
}


// Отрисовка fps в title
void GameRender::drawFps(sf::RenderWindow& window, float fps)
{
	char c[16];
	snprintf(c, 16, "FPS: %f", fps);
	sf::String str(c);
	window.setTitle(str);
}

// Отрисовка шаров в окне
void GameRender::drawBall(sf::RenderWindow& window, GameModel::Ball& ball)
{
	gball.setRadius(ball.m_radius);
	gball.setOrigin(ball.m_radius, ball.m_radius);
	gball.setPosition(ball.m_position.x, ball.m_position.y);
	gball.setFillColor(sf::Color(
		255 - ball.m_collision_count,
		255 - ball.m_collision_count,
		255 - ball.m_collision_count));

	window.draw(gball);
}

// Основной цикл прорисовки на экран
void GameRender::render() {
	current_time = clock.getElapsedTime();
	fpscounter.push(1.f / (current_time.asSeconds() - last_time.asSeconds()));

	m_window.clear(sf::Color(25, 125, 25));
	for (auto& ball : m_model->getBalls())
	{
		m_model->moveBall(ball, current_time.asSeconds() - last_time.asSeconds());
		drawBall(m_window, ball);
	}
	m_model->checkCollision();

	drawFps(m_window, fpscounter.getAverage());
	m_window.display();

	last_time = current_time;
}

// Перезапуск таймера
void GameRender::clockRestart() {
	clock = sf::Clock::Clock();
	last_time = clock.getElapsedTime();
}