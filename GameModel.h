#pragma once

#include <SFML/Graphics.hpp>

namespace constants 
{
	static float const pi = atan(1) * 4.;
}

class GameModel final
{
private:
	static constexpr int WINDOW_X = 1024;
	static constexpr int WINDOW_Y = 768;
	static constexpr int MAX_BALLS = 50; // 300
	static constexpr int MIN_BALLS = 50; // 100

	static_assert(WINDOW_X >= 0, "GameModel.h ! WINDOW_X is negative");
	static_assert(WINDOW_Y >= 0, "GameModel.h ! WINDOW_Y is negative");
	static_assert(MIN_BALLS <= MAX_BALLS, "GameModel.h ! MIN_BALL > MAX_BALLS");
	static_assert(MIN_BALLS >= 0, "GameModel.h ! MIN_BALL is negative");
	static_assert(MAX_BALLS >= 0, "GameModel.h ! MAX_BALLS is negative");

	static float getRandomNumber(const int& from, const int& to) {
		return rand() % (to - from + 1) + from;
	}

public:
	struct Ball
	{
		int m_radius = getRandomNumber(5, 10);
		sf::Vector2f m_position = sf::Vector2f(
			getRandomNumber(m_radius * 2.f, WINDOW_X - m_radius * 2.f),
			getRandomNumber(m_radius * 2.f, WINDOW_Y - m_radius * 2.f));
		sf::Vector2f m_direction = sf::Vector2f(
			getRandomNumber(-5, 5) / 3.f,
			getRandomNumber(-5, 5) / 3.f);
		float m_speed = getRandomNumber(30.f, 60.f); // 30 60
		float m_collision_count = 0;
	};

private:
	std::vector<Ball> m_balls = {};

public:
	GameModel()
	{
		init();
	}
	~GameModel()
	{
	}
	GameModel(const GameModel&) = delete;
	GameModel& operator=(const GameModel&) = delete;

	const int& getWindow_X() const;
	const int& getWindow_Y() const;
	const float& getDinstance(Ball& ball_a, Ball& ball_b) const;
	std::vector<Ball>& getBalls();
	bool init();
	void moveBall(Ball& ball, const float& delta_time);
	void wallsCollision(Ball& ball);
	void ballsCollision();
	void checkCollision();
};