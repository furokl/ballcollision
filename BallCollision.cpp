#include "MiddleAverageFilter.h"
#include "GameModel.h"
#include "GameRender.h"
#include "GameController.h"

/*
* Created by Logunov on 10/06/2022
* Ball Collision
* @comment: Test task
*/

int main()
{	
    GameModel model;
    GameRender render(&model);
    GameController controller(&model, &render);
    controller.run();
}